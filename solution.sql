-- 1 Return the customerName of the cutsomers who are from Philippines

SELECT * FROM customers WHERE country = "Philippines";

-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"

SELECT customers.contactLastName, customers.contactFirstName
	FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3. Return the product name and MSRP of the Product named "The Titanic"

SELECT products.productName, products.MSRP
	FROM products WHERE productName = "The Titanic";

-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com";

SELECT employees.firstName, employees.lastName
	FROM employees WHERE email = "jfirrelli@classicmodelcars.com";


-- 5. Return the names of the customers who have no registered state
SELECT customers.customerName 
	FROM customers WHERE state = NULL;

-- 6. Return the first name , last name , email of the employee whose last name is Patterson and fistname is Steve

SELECT employees.firstName, employees.lastName, employees.email
	FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";


-- 7. Return customer name  , country and credit limit of customers whose country are NOT USA and whose credit limit are greater than 3000
SELECT customers.customerName, customers.country, customers.creditLimit
	FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8. Return customer name of customers whose customer name don't have 'a' in them
SELECT customers.customerName FROM customers WHERE customerName NOT LIKE "%a%";

-- 9. Return the customer numbers of orders whose comment contain a string 'DHL'
SELECT customers.customerNumber FROM customers
	JOIN orders ON customers.customerNumber = orders.customerNumber
	WHERE orders.comments LIKE "%DHL%";


-- 10. Return the product lines whose text description mentions the phrase 'state of heart';

SELECT productlines.productLine 
	FROM productlines
	WHERE textDescription LIKE "%state of heart%";

-- 11. Return the countries of customers without duplication
SELECT DISTINCT country FROM customers;


-- 12. Return the statuses of orders without duplication
SELECT DISTINCT status FROM orders;

-- 13. Return the customers name and countries of customer whose country is USA, FRANCE , or CANADA
SELECT customers.customerName, customers.country
	 FROM customers WHERE country IN ("USA","FRANCE","CANADA");

-- 14. Return the first name, last name ,and office's city of employee whose office are in Tokyo
SELECT employees.firstName, employees.lastName, offices.city FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode
	WHERE city = "Tokyo";

-- 15. Return the customer names of customers who were served by the employee named "Leslie Thompson"
SELECT customers.customerName 
	FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	WHERE employees.lastName = "Thompson" AND employees.firstName = "Leslie";

-- 16. Return the product names and customer of product ordered by "Baane Mini Imports"
SELECT products.productName , customers.customerName
	FROM customers
	JOIN orders ON customers.customerNumber = orders.customerNumber
	JOIN products
	WHERE customerName = "Baane Mini Imports";


-- 17. Return the employee's first names, employee's last name, customer's name , and office's country of transaction whose customers and offices are in the same country
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country
	FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode
	JOIN customers
	WHERE customers.country = offices.country;


-- 18. Return the last names and first names of employees being supervised by "Anthony Bow"
SELECT employees.lastName, employees.firstName
	FROM employees
	WHERE employees.reportsTo = 1143;

-- 19. Return the product name and MSRP of the product with the highest MSRP
SELECT MAX(MSRP)
	FROM products;


-- 20. Return the number of customers in the UK
SELECT COUNT(*)
	FROM customers WHERE country = "UK";

-- 21. Return the number of produtcs per product line
SELECT COUNT(DISTINCT productLine) as "Number of products by product lines" FROM products;3

-- 22. Return the number of customers served by every employee.
SELECT COUNT(DISTINCT salesRepEmployeeNumber) as "Number of customers" FROM customers;


-- 23. Return the product name and quantity in stocks of products that belong to the product line "planes" with stock quantities less than 1000
SELECT products.productName, products.quantityInStock 
	FROM products
	WHERE productLine = "Planes" AND quantityInStock < 1000;